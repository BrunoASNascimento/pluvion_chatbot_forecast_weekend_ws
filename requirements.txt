# Function dependencies, for example:
# package>=version
pymongo==3.6.1
json-encoder==0.4.4
pandas==0.24.2
DateTime==4.3
Flask==1.0.3
numpy==1.16.3